# Install
Install gcloud at https://cloud.google.com/sdk/docs/downloads-apt-get
* ref: https://cloud.google.com/sdk/gcloud/reference

# Run init
Run `gcloud init` on terminal and login with your account and press allow
Comeback terminal and type `n` then enter

# Run bash script and get key
* bash customsearch.sh email_adress name
* example: bash customsearch.sh example@gmail.com yourtaskname
* Type interger number to setup your config & choose your project
* When `Checking network connection` orcur, get key starting...

# Error
* accept Terms of Service : access https://console.cloud.google.com/home/dashboard to accept and retry.

# For IAM service account
please read at https://cloud.google.com/iam/docs/creating-managing-service-account-keys

